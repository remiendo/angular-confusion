import { Injectable } from '@angular/core';
import { Leader } from '../shared/Leader';
import { LEADERS } from '../shared/Leaders';

@Injectable({
  providedIn: 'root'
})
  export class LeaderService {

    constructor() { }

    getLeaders(): Promise<Leader[]> {
      return new Promise(resolve =>{
        setTimeout(() => resolve(LEADERS), 5000);
        
      });
    }
    getLeader(id: string): Promise<Leader> {
      return new Promise(resolve=> {
        setTimeout(()=> resolve(LEADERS.filter((leader) => (leader.id === id))[0]),5000);

      });
    }
    getFeaturedLeader(): Promise<Leader> {
      return new Promise(resolve=>{
        setTimeout(() => resolve(LEADERS.filter((leader) => leader.featured)[0]), 5000);
      });
    }
    
  }
